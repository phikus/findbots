#!/usr/bin/env bash

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
  echo "Amazing script for finding bots in your logs"
  echo -e "Usage:\n$(basename $0) file.log\nOR\ncat file.log | $(basename $0)"
  echo "Option -v adds more verbosity(used for debug)"
  exit 0
fi

if [[ "$1" == "-v" ]]; then
  set -x
  export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
  shift
fi

# Sample sequence we are looking for
#                0               |      1      |     2      | 3 |      4       | 5
# Mon, 22 Aug 2016 13:15:39 +0200|178.57.66.225|fxsciaqulmlk| - |user logged in| -
# Mon, 22 Aug 2016 13:15:39 +0200|178.57.66.225|fxsciaqulmlk| - |user changed password| -
# Mon, 22 Aug 2016 13:15:39 +0200|178.57.66.225|fxsciaqulmlk| - |user logged off| -

# User-friendly indexes
LOG_TIMESTAMP=0
LOG_PROFILE=2
LOG_ACTION=4

# Actions strings
ACTION_USER_LOGIN='user logged in'
ACTION_USER_CHANGED_PASS='user changed password'
ACTION_USER_LOGOFF='user logged off'

function reset_vars() {
 USER_PROFILE=''
 IS_LOGGED_IN=''
 IS_PASSWORD_CHANGED=''
 START_TIMESTAMP=''
 END_TIMESTAMP=''
}
reset_vars

while read line
do
#  echo
#  echo "$line"
  readarray -td '|' log_line < <(printf '%s' "$line")

  if ! [[ "${log_line[$LOG_ACTION]}" =~ ($ACTION_USER_LOGIN|$ACTION_USER_CHANGED_PASS|$ACTION_USER_LOGOFF) ]]; then
    reset_vars
    continue
  fi

  if [[ "${log_line[$LOG_ACTION]}" == "$ACTION_USER_LOGIN" ]]; then
    USER_PROFILE=${log_line[$LOG_PROFILE]}
    START_TIMESTAMP=${log_line[$LOG_TIMESTAMP]}
    IS_LOGGED_IN=1
    continue
  fi

  if [[ ${log_line[$LOG_PROFILE]} == "$USER_PROFILE" ]] && [[ $IS_LOGGED_IN ]]; then
    if [[ "${log_line[$LOG_ACTION]}" == "$ACTION_USER_CHANGED_PASS" ]]; then
      IS_PASSWORD_CHANGED=1
      continue
    fi
  fi

  if [[ ${log_line[$LOG_PROFILE]} == "$USER_PROFILE" ]] && [[ $IS_PASSWORD_CHANGED ]]; then
    if [[ ${log_line[$LOG_ACTION]} == "$ACTION_USER_LOGOFF" ]];then
      END_TIMESTAMP=${log_line[$LOG_TIMESTAMP]}
      DATE_DIFFERENCE=$(printf "%s\n" $(( $(date -d "$END_TIMESTAMP" "+%s") - $(date -d "$START_TIMESTAMP" "+%s") ))) 
      # if difference between start and end timestamps within 1 second then it's a bot
      (( DATE_DIFFERENCE < 1 )) && echo "$USER_PROFILE is a bot"

      reset_vars
    fi
 fi

done < "${1:-/dev/stdin}"
